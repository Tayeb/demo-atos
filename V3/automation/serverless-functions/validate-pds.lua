


local pds = kong.request.get_header("X-Artemis-pds");
local http=require("socket.http");
local ltn12 = require("ltn12");
local responseMessage = "";
local response_body = {}


if not pds then 
	return kong.response.exit(400, "Bad Resquest, header X-Artemis-pds doesn't exist");
end 

http.request{
    url = "http://172.17.0.1:8080/validate",
    method = "GET", 
    headers = 
      {
          ["X-Artemis-pds"] = pds;
      },
      sink = ltn12.sink.table(response_body),
}

if type(response_body) == "table" then
  responseMessage = table.concat(response_body)
  if responseMessage == "KO" then 
     return kong.response.exit(403, "Forbidden, Request header not valid")
  end
else
  return kong.response.exit(500, "Error occur on server")
end





