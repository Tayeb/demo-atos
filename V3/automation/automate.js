var fs = require('fs');
var request = require('request');

const API_SPECS_FILE = fs.readFileSync(process.env.SPECS_FILE).toString();
const SERVERLESS_FUNCTION_VALIDATE_PDS = fs.readFileSync('./serverless-functions/validate-pds.lua').toString();
const KONG_ADMIN_ENDPOINT = process.env.KONG_HOST_URL
const BASE_PATH = process.env.BASE_PATH
const PROTO = process.env.PROTO
const ROUTE = process.env.ROUTE
const SERVICE = process.env.SERVICE

var service = {};
var route = {};
var plugin = {};

const apiSpecsObj = JSON.parse(API_SPECS_FILE);


function createService() {
	if (apiSpecsObj.swagger) {
		const URL_ENDPOINT = KONG_ADMIN_ENDPOINT + '/services';
			request.post(URL_ENDPOINT, {
				form: {
					name: apiSpecsObj.info.title.replace(/ /g, '-'),
					url: getServiceURL()
				}
			}).on('response', function (response) {
				let res;
				response.on('data', function (data) {
					res += data.toString()
				})
				response.on('end', function () {
					res = res.slice(9);
					service = JSON.parse(res);
					console.log("service created")
					console.log(service)
					addRoute(service);
				})
			})
	} else if (apiSpecsObj.openapi) {
		apiSpecsObj.servers.forEach(function (server, index) {
			request.post(KONG_ADMIN_ENDPOINT + '/services', {
				form: {
					name: apiSpecsObj.info.title.replace(/ /g, '-') + '-' + index,
					url: getServiceURL(server)
				}
			}).on('response', function (response) {
				let res;
				response.on('data', function (data) {
					res += data.toString();
				})
				response.on('end', function () {
					res = res.slice(9);
					service = JSON.parse(res);
					console.log('service created');
					console.log(service);
					addRoute(service)
				})
			})
		})
	} else {
		console.log("Please specify an Open Api specification file !");
	}
}

function addRoute(service, basePath) {
	const URL_ENDPOINT = KONG_ADMIN_ENDPOINT + '/services/' + service.name + '/routes';
	Object.keys(apiSpecsObj.paths).forEach(function (path, index) {
		request.post(URL_ENDPOINT, {
			json: true,
			body: {
				name: path.replace(/\//g, '_').replace(/{id}/g, '') + '_endpoint_' + index,
				paths: new Array(path.replace(/{id}/g, '\\w')),
				methods: Object.keys(apiSpecsObj.paths[path]).filter(function (method) {
					return ((method === 'get') || (method === 'post') || (method === 'put') || (method === 'delete'))
				}).map(function (method) {
					return method.toUpperCase()
				}),
				strip_path: false
			}
		}).on('response', function (response) {
			let res;
			response.on('data', function (data) {
				res += data.toString();
			})
			response.on('end', function () {
				res = res.slice(9);
				route = JSON.parse(res);
				console.log('route added');
				console.log(route);
			})
		})
	})
}

function enablePlugin() {
	if (ROUTE) {
		const route = JSON.parse(ROUTE);
		const URL_ENDPOINT = KONG_ADMIN_ENDPOINT + '/routes/' + route.name + '/plugins';
		request.post(URL_ENDPOINT, {
			form: {
				'name': 'pre-function',
				'config.functions': SERVERLESS_FUNCTION_VALIDATE_PDS
			}
		}).on('response', function (response) {
			let res;
			response.on('data', function (data) {
				res += data.toString();
			})
			response.on('end', function () {
				res = res.slice(9);
				plugin = JSON.parse(res);
				console.log('plugin enabled');
				console.log(plugin)
			})
		})
	} else if(SERVICE){
		const service = JSON.parse(SERVICE)
		const URL_ENDPOINT = KONG_ADMIN_ENDPOINT + '/services/' + service.name + '/plugins';
		request.post(URL_ENDPOINT, {
			form: {
				'name': 'pre-function',
				'config.functions': SERVERLESS_FUNCTION_VALIDATE_PDS
			}
		}).on('response', function (response) {
			let res;
			response.on('data', function (data) {
				res += data.toString();
			})
			response.on('end', function () {
				res = res.slice(9);
				plugin = JSON.parse(res);
				console.log('plugin enabled');
				console.log(plugin)
			})
		})
	} else  {
		const URL_ENDPOINT = KONG_ADMIN_ENDPOINT + '/plugins';
		request.post(URL_ENDPOINT, {
			form: {
				'name': 'pre-function',
				'config.functions': SERVERLESS_FUNCTION_VALIDATE_PDS
			}
		}).on('response', function (response) {
			let res;
			response.on('data', function (data) {
				res += data.toString();
			})
			response.on('end', function () {
				res = res.slice(9);
				plugin = JSON.parse(res);
				console.log('plugin enabled');
				console.log(plugin)
			})
		})
	}
}

function getServiceURL(server) {
	if (apiSpecsObj.swagger) {
		return (PROTO ? PROTO : 'https') + '://' + apiSpecsObj.host + '' + (BASE_PATH ? BASE_PATH : (apiSpecsObj.basePath ? apiSpecsObj.basePath : '/'))
	} else {
		if(server.url.includes('{port}') && server.url.includes('{basePath}')){
			let url;
			url = server.url.replace(/{port}\//g, server.variables.port.default).replace(/{basePath}/g, (BASE_PATH ? BASE_PATH : (server.variables.basePath.default ? server.variables.basePath.default : '/')))
			return url;
		}else{
			return server.url + (BASE_PATH ? BASE_PATH : '/');
		}
	}
}

createService();
enablePlugin();